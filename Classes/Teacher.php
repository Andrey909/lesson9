<?php

class Teacher extends Person{
    public $subject;

    public function __construct($id, $full_name, $phone, $email, $role, $subject){
        parent::__construct($id, $full_name, $phone, $email, $role);
        $this->subject= $subject;
    }

   public function getVisitCard(){
   return '<br>'.$this->id.' - '.$this->full_name.' <br> '.$this->role.' <br> тел. '.$this->phone.' <br> '.$this->email.' <br> преподаваемый предмет '.$this->subject;}}