<?php
class Admin extends Person{
    public $working_day;

    public function __construct($id, $full_name, $phone, $email, $role, $working_day){
        parent::__construct($id, $full_name, $phone, $email, $role);
        $this->working_day = $working_day;
    }

   public function getVisitCard(){
   return '<br>'.$this->id.' - '.$this->full_name.' <br> '.$this->role.' <br> тел. '.$this->phone.' <br> '.$this->email.' <br>день работы: '.$this->working_day;}
}


