<?php
header('Content-Type: text/html; charset=utf-8');
require_once 'connect_db.php';
try{
	$sqlQuery = '
		CREATE TABLE members(
			id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
			full_name VARCHAR(255),
			phone VARCHAR(255),
			email VARCHAR(255),
			role VARCHAR(255),
			averange_mark Float(24),
			subject VARCHAR(255),
			working_day VARCHAR(255)
		) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;
	';
	$mydb->exec($sqlQuery);
}catch(PDOException $e){
	die('Не удалось создать таблицу members!<br>'.$e->getMessage());
}

